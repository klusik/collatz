/*
    Collatz

    Author:         klusik@klusik.cz

    Description:    Creates a list of numbers limited by user input, in which
                    there are all numbers investigated as in collatz conjecture,
                    meaning if the number is odd, it's multiplied by 3 and added 1,
                    if the number is even, it's just divided by two.

                    The goal is to track a path to "one." Collatz conjecture says for
                    every given natural number there is finite number of steps to go back to
                    one in this way.
*/
#include <stdio.h>
#include <stdlib.h>

/* Create a structure for attempts */
typedef struct attempts {
    int attempt;

    struct attempts *next;
} ATTEMPTS;

/* Create a structure for a list of numbers */
typedef struct numbers {
    int number;

    ATTEMPTS *attempts;
    struct numbers *next;
} NUMBERS;


/**
    Returns a list of attempts for given number
*/
ATTEMPTS *createAttempts(int number) {
    ATTEMPTS *newAttempt;
    ATTEMPTS *firstAttempt, *lastAttempt;
    int actualNumber;

    firstAttempt = lastAttempt = NULL;

    /* Initial number setup */
    actualNumber = number;

    while(actualNumber > 1)
    {
        /* Memory stuff */
        newAttempt = NULL;
        if (!(newAttempt = (ATTEMPTS *) malloc(sizeof(ATTEMPTS))))
        {
            return NULL;
        }

        newAttempt->attempt = actualNumber;
        newAttempt->next = NULL;

        if (firstAttempt == NULL)
        {
            /* First attempt */
            firstAttempt = newAttempt;
            lastAttempt = firstAttempt;
        }
        else
        {
            /* Not first */
            lastAttempt->next = newAttempt;
            lastAttempt = lastAttempt->next;
        }

        /* Math stuff */
        if ((actualNumber%2) == 0)
        {
            /* Even */
            actualNumber = actualNumber / 2;
        }
        else
        {
            /* Odd */
            actualNumber = (3 * actualNumber) + 1;
        }
    }

    return(firstAttempt);
}

/**
    Creates a list of numbers and attempts to go back to one.
    Returns a pointer to NUMBERS structure.
*/
NUMBERS *createList(int maxNumber) {
    ATTEMPTS *attempts;
    NUMBERS
            *newNumber = NULL,
            *lastNumber = NULL,
            *firstNumber = NULL;

    int index;

    for (index = 2; index <= maxNumber; index++){


        if (!(newNumber = (NUMBERS *) malloc(sizeof(NUMBERS))))
        {
            return NULL;
        }
        /* printf("Exploring number %d.\n", index);*/

        if (!(attempts = createAttempts(index)))
        {
            return NULL;
        }

        newNumber->attempts = attempts;
        newNumber->number = index;
        newNumber->next = NULL;

        /* Attempts created, now save them into the list of attempts */
        if (firstNumber == NULL)
        {
            /* First */
            firstNumber = lastNumber = newNumber;
        }
        else
        {
            /* Not first */
            lastNumber->next = newNumber;
            lastNumber = lastNumber->next;
        }

    }

    return firstNumber;
}

/**
    Reads a whole list of numbers
*/
int printList(NUMBERS *firstNumber)
{
    NUMBERS *reader = firstNumber;
    ATTEMPTS *attReader = NULL;
    FILE *fwOutput;

    fwOutput = fopen("output.txt", "w");

    while(reader != NULL)
    {
        fprintf(fwOutput, "%d: ", reader->number);

        /* For this number lets go through all the generated numbers */
        attReader = reader->attempts;
        attReader = attReader->next;
        while(attReader != NULL)
        {
            fprintf(fwOutput, "%d, ", attReader->attempt);
            attReader = attReader->next;
        }
        fprintf(fwOutput, "\n");

        reader = reader->next;
    }

    fclose(fwOutput);

    return(0);
}

int main(int argc, char **argv)
{
    int maxNumber;
    NUMBERS *listStart;

    printf("Enter a maximal number: ");
    scanf("%d", &maxNumber);

    listStart = createList(maxNumber);

    printList(listStart);

    return 0;
}
